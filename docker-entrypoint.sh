#!/bin/bash

if [ ! -f /app/thumbor.conf ]; then
  envtpl /app/thumbor.conf.tpl  --allow-missing --keep-template
fi

# If log level is defined we configure it, else use default log_level = info
if [ -n "$LOG_LEVEL" ]; then
    LOG_PARAMETER="-l $LOG_LEVEL"
fi

# Check if thumbor port is defined -> (default port 8080)
if [ -z ${THUMBOR_PORT+x} ]; then
    THUMBOR_PORT=8080
fi

        echo "---> Starting thumbor solo..."
        exec python -m thumbor/server --port=$THUMBOR_PORT --conf=/app/thumbor.conf $LOG_PARAMETER

exec "$@"
